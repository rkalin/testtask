﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EmployeeForHTC.Models;
using EmployeeForHTC.SearchClass;

namespace EmployeeForHTC.Controllers
{
    public class HomeController : Controller
    {
        EmplContext DbEmpl = new EmplContext();
        // GET: Home
        public ActionResult Index()
        {
            IEnumerable<Employee> employees = DbEmpl.Employees;
            ViewBag.Empl_s = employees;
            return View();
        }

        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(Employee EmploOb)
        {
            DbEmpl.Employees.Add(EmploOb);
            DbEmpl.SaveChanges();

            return RedirectToAction ("Index");
        }
        [HttpGet]
        public ActionResult Delete()
        {
               return View();
        }
        [HttpPost, ActionName("Delete")]
       public ActionResult Deletee(string name)
        {
           Employee emp = DbEmpl.Employees
                .Where(p => p.Name == name)
                .FirstOrDefault();

            DbEmpl.Employees.Remove(emp);
            DbEmpl.SaveChanges();
            return RedirectToAction("Index");
        }
        [HttpPost]
        public ActionResult Search(EmplSearch searchModel)
        {
            var search = new SearchCl();
            var model = search.GetEmployee(searchModel);
            return View(model);
        }
    }
}