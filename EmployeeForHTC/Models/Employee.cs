﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace EmployeeForHTC.Models
{
    public class Employee
    {
        [Key]
        public int EmployeeID { get; set; }
        public string Surname { get; set; }
        public string Name { get; set; }
        public string Patronymic { get; set; }
        public string DateOfBirth { get; set; }
        public string Organization { get; set; }
        public string Position { get; set; }
        public string ContectInformation { get; set; }
        public long PhoneNumber { get; set; }
        public string Email { get; set; }
        public string Skype { get; set; }
        public string Other { get; set; }
    }

    public class EmplSearch
    {
        public int? EmployeeID { get; set; }
        public string Surname { get; set; }
        public string Name { get; set; }
        public string Patronymic { get; set; }
        public string DateOfBirth { get; set; }
        public string Organization { get; set; }
        public string Position { get; set; }
        public string ContectInformation { get; set; }
        public long? PhoneNumber { get; set; }
        public string Email { get; set; }
        public string Skype { get; set; }
        public string Other { get; set; }

    }

}